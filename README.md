**This build of dwm of part of SÅMDE, it's a arch linux post installation script that recreates my desktop setup. I highly recommend checking it out : [SÅMDE](https://gitlab.com/SamDenton/samde)**

# SÅM's build of dwm
[Dwm](https://dwm.suckless.org/) is the Dynamic Window Manager made by suckless. I've added quite a few patches and added some of my own modifications to the source code to implement the features that I want.

# Stuff I've added
These are things that I've added myself:

- Added the ability to make programs sticky by default in the rules array.
- Each program can use one of the 6 bright colors from the selected [Xresources colorscheme](https://gitlab.com/SamDenton/dots/-/tree/master/.config/x11/colorschemes) as its border color. The selected color is defined in the rules array in config.def.h.

# Patches
Here is the full list of the patches I've applied:

- actualfullscreen
- alwayscenter
- aspectresize
- attachbottom
- bar-height
- barpadding
- centretitle
- colorfultags
- fixborders
- fullgaps
- moveresize
- movetoedge
- preserveonrestart
- scratchpad
- status2d
- sticky
- underlinetags
- viewonrulestag
- xresources

# Installation
```
git clone --depth 1 https://gitlab.com/SamDenton/dwm.git
cd dwm
sudo make install
```
